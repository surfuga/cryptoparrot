# -*- coding: utf-8 -*-

import random
import string
import yaml
import tweepy

import textGeneration


def getContent():
  contentfile = open('content.yml','r')
  data = yaml.load(contentfile)
  contentfile.close()

  llog = data['content']
  account = data['account']

  return llog, account


def get_api():

  cfgfile = open('cfg.yml','r')
  cfg = yaml.load(cfgfile)
  cfgfile.close()


  auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
  auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
  return tweepy.API(auth)


def tweet(publication):

  api = get_api()
  status = api.update_status(status=publication)



if __name__ == '__main__':

  llog, account = getContent()

  publication = textGeneration.textGeneration(llog, account)

  if publication:
    tweet(publication)

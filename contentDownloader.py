# -*- coding: utf-8 -*-
from urllib import urlopen
from bs4 import BeautifulSoup
import feedparser
import yaml
import random


# 0-feed sources, read from file!
feedfile = open('feeds.yml','r')
data = yaml.load(feedfile)
feedfile.close()
choosen_feed = random.choice(data['urls'])

# 1-feedparser download
url = choosen_feed[choosen_feed.keys()[0]]
twitter_account = choosen_feed.keys()[0]
llog = feedparser.parse(url)

# 2-save to file as a json?
contentfile = open('content.yml','w')
data = {
  'account' : twitter_account,
  'content': llog
  }
yaml.dump(data, contentfile)

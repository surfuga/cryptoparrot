# -*- coding: utf-8 -*-

from __future__ import division
import nltk, re, pprint
from nltk import word_tokenize

from urllib import urlopen
from bs4 import BeautifulSoup

import nltk
import operator
import random
import string
import yaml


punctuation_set = set(['.',',',';',':','!','?','¡','¿'])


"""

1. twitter account that every day chooses a random blog  (or tweet account) and generates_text -> 10-30 chars
    . publish 3 per day!
    . add footer saying these are not his/her words, but algorithm generated

5. Twitter accounts
  . cryptoparrot <- for security
  . polòniaParrot (easy to spot... dont)
  . politicalParrot
  . twitterparrot

  . how to tweet from app -> register twitter app
    -> done!

2. Internals:
    1.add starting word calculation (get most 10 frequent words(no stop words? or yes?) and start with one random choice of them)
      -> done!

    2. loop detection and recalculation
      -> done!

    10. read from file
      - extract link and tweeter account from file -> done!
      - read content from file!
      -> done!

    7. header and footer:
      - header: add twitter account link or blog link "Today i feel like @TARGET: "
      - Footer: "Disclaimer: this is an automated and random text generation. None of these word where said/written by the blogs/persons mentioned/linked on each tweet. This work is based on NLTK book."
      -> done!

    11. improve punctuation detection
        -  end with ,.
        - words with punctuation are not well categorized for space separation U.S., low-security
        - can't, don't, would'nt,
        -> done!

    9. twitter publishing through app
      -> done!

    8. automated services (downloader, publisher) in raspberry pi: systemd version
      - asynchronous content downloading to avoid detection
      - no every day downloads
      - no at the same time
      - just download once per day...
      - random blog choosing
      - 2nd service that does the download and saves to a text file
      - first service reads that file, extracts name and links and content

      0- pyenv and virtualenv!
      1- install services/timers/scripts
      2- downloader.sh and publisher.sh
      3- tweek publisher to only write to a log
      4- timers adjust to be asynchrnous and a bit random

    3. verification by telegram bot

    12. add tons of feeds for source

    6. from rss feed to full post?...

    7. code source in github (only the publisher..) ...

    8. twitter account as a source...

"""



def pick_start_word(cfdist, n=10):
  """
    random choice between the top 10 most frequent words

    ->eliminate stop words or include them?
  """

  sorted_cfdist = sorted(cfdist.iteritems(), key=operator.itemgetter(1), reverse=True)
  choice = random.choice(sorted_cfdist[:10])

  return choice[0]



def pick_word_from_model(cfdist,word="", n=5):
  likely_words = []

  # get all companion words
  companion_words = cfdist[word]

  # sort them in reverse order
  current_sfd = sorted(cfdist[word].iteritems(), key=operator.itemgetter(1), reverse=True)

  # the n first of them
  if len(current_sfd) >= n :
    likely_words = current_sfd[:n]
  else:
    likely_words = current_sfd

  # choose the next word
  if len(likely_words)>0:
    word = random.choice(likely_words)[0]
    return word
  else:
    return ''

def write_word (sentence, word):
  # ending avoiding stopwords

  if sentence == "":
    sentence += word.capitalize()
  #elif word.isalnum() or word.isdigit() or word not in punctuation_set:
  elif word.isalnum() or word.isdigit() or word.find("-") > -1 or word.startswith("``") :
    sentence += " " + word
  else:
    sentence += word

  return sentence

def detect_loops(text):
  tokens = word_tokenize(text)
  bigrams = nltk.bigrams(tokens)
  fdist = nltk.FreqDist(bigrams)
  sorted_fdist = sorted(fdist.iteritems(), key=operator.itemgetter(1), reverse=True)
  if sorted_fdist[0][1] > 1:
    return True
  else:
    return False


def generate_model(cfdist, word, iterations=15, n=5):

  result = ""

  previous_word = ''
  for i in range(iterations):
    result = write_word(result,word)
    previous_word = word
    word = pick_word_from_model(cfdist,word, n)


  # ending avoiding stopwords and , ; :
  while previous_word in nltk.corpus.stopwords.words('english') or \
        previous_word == "," or \
        previous_word == ";" or \
        previous_word == ":":

    word = pick_word_from_model(cfdist,previous_word, n)

    if word in punctuation_set:
      continue

    result = write_word(result,word)
    previous_word = word

  find_inclined_double_quotes = result.find("``")
  find_double_quotes = result.find("''")

  if find_inclined_double_quotes > 0 and \
    result[find_inclined_double_quotes:].find("''") == -1:
    result = write_word(result,"''")
  elif find_inclined_double_quotes == -1 and \
      find_double_quotes > 0:
    result = write_word(result,"''")

  if previous_word != ".":
    result = write_word(result,".")

  return result


def generate_model_without_word(cfdist, iterations=15, n=5):

  word = pick_start_word(cfdist=cfdist, n=n)
  result =  generate_model(cfdist,word,iterations,n)
  tries = 0
  while  detect_loops(result) and tries < 10:
    word = pick_start_word(cfdist=cfdist, n=n)
    result =  generate_model(cfdist,word,iterations,n)
    tries += 1

  return result

def censorship(publication):
  #apply some censorship
  censorfile = open('feeds.yml','r')
  data = yaml.load(censorfile)
  censorfile.close()

  censorship = data['censorship']

  tokens = word_tokenize(publication)
  found = [ w for w in tokens if w.lower() in censorship]
  if len(found) > 0:
    print 'refusing to publish' + publication
    return None
  else:
    print 'accepted to publish' + publication
    return publication


def textGeneration(llog, account):

  # 1-prepare tokens_list
  tokens = []

  # 2-for each blog entry, get raw content and tokenize,
  #   and add to tokens



  for post in llog['entries']:

    if 'content' in post.keys():
      content = post['content'][0].value
    elif 'summary_detail' in post.keys():
      content = post['summary_detail'].value

    raw = BeautifulSoup(content,"html5lib").get_text()
    new_tokens = word_tokenize(raw)
    tokens.extend(new_tokens)

  # 3-call gentext function
  n = 7
  iterations = 12

  bigrams = nltk.bigrams(tokens)
  cfd = nltk.ConditionalFreqDist(bigrams)
  sentence = generate_model_without_word(cfd,iterations,n)

  publication = "Today I'm parodying " + \
                account + ": " +\
                sentence


  publication = censorship(publication)

  return publication
